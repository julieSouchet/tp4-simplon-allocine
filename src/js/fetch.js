import {apiKey} from '../../assets/apiKey.js';
import {searchWrapper, carouselWrapper, genreWrapper} from './index.js';

// --------------------------//
//   QUERY TMDB FOR MOVIES   //
// --------------------------//
const fetchMovies = () => {
  // look for the movie with this name in the TMDb database
  fetch(`https://api.themoviedb.org/3/search/movie?api_key=${apiKey}&query=${searchWrapper.name}&page=${searchWrapper.page}&language=fr`)
      .then((response) => {
      // reads the answer and converts it to JSON
        if (response.ok) {
          return response.json();
        } else {
          throw new Error(response.status);
        }
      })
      // if all was ok
      .then((jsonResponse) => {
      // parse the JSON for the relevant bits
        searchWrapper.nbResults = jsonResponse.total_results;
        searchWrapper.resultList = jsonResponse.results;
        searchWrapper.nbPages = jsonResponse.total_pages;
      },
      // if there was an error
      (error) => {
      // we ignore errors of the type "empty query"
        if (error.message == 422) {
          searchWrapper.nbResults = -1;
          searchWrapper.resultList = [];
        } else {
          console.error(error);
        }
      });
};

// const fetchMovieDetails = (id) => {
//   fetch(`https://api.themoviedb.org/3/movie/${id}?api_key=${apiKey}`)
//       .then((response) => {
//         // reads the answer and converts it to JSON
//         if (response.ok) {
//           return response.json();
//         } else {
//           throw new Error(response.status);
//         }
//       });
// };

const fetchTrending = () => {
  fetch(`https://api.themoviedb.org/3/trending/movie/week?api_key=${apiKey}&language=fr`)
      .then((response) => {
        // reads the answer and converts it to JSON
        if (response.ok) {
          return response.json();
        } else {
          throw new Error(response.status);
        }
      })
      .then((jsonResponse) => {
        carouselWrapper.movieList = jsonResponse.results;
      });
};

const fetchGenreTrending = (genre) => {
  fetch(`https://api.themoviedb.org/3/discover/movie?api_key=${apiKey}&language=fr&sort_by=popularity.desc&include_adult=false&include_video=false&page=1&with_genres=${genre}`)
      .then((response) => {
        // reads the answer and converts it to JSON
        if (response.ok) {
          return response.json();
        } else {
          throw new Error(response.status);
        }
      })
      .then((jsonResponse) => {
        genreWrapper.movieList = jsonResponse.results;
      });
};


// ---- fetch the list of all genres ----
const emptyGenre = {name: '', id: 0};
let genreList = [];
// there are 19 main genres in TMDb
for (let i = 0; i < 19; i++) {
  genreList.push(emptyGenre);
}

fetch(`https://api.themoviedb.org/3/genre/movie/list?api_key=${apiKey}&language=fr`)
    .then((response) => {
    // reads the answer and converts it to JSON
      if (response.ok) {
        return response.json();
      } else {
        throw new Error(response.status);
      }
    })
// if all was ok
    .then((jsonResponse) => {
    // parse the JSON for the relevant bits
      genreList = jsonResponse.genres;
    },
    // if there was an error
    (error) => {
      console.error(error);
    });

// initializing the movie list
const emptyMovie = {
  'adult': false,
  'backdrop_path': '',
  'genre_ids': [],
  'id': 0,
  'original_language': '',
  'original_title': '',
  'overview': '',
  'poster_path': '',
  'release_date': '',
  'title': '',
  'video': false,
  'vote_average': 0,
  'vote_count': 0,
  'popularity': 0,
};

const emptyMovieList = [];
// 20 is the numbers of results of fetch
for (let i = 0; i < 20; i++) {
  emptyMovieList.push(emptyMovie);
}


export {fetchMovies, fetchTrending, fetchGenreTrending,
  emptyMovie, emptyMovieList, genreList};
