import noUiSlider from 'nouislider';
import 'nouislider/distribute/nouislider.css';
import searchWrapper from './index.js';
import {yearMin, yearMax} from './constants.js';

// ------ YEAR FILTER slider ------
const slider = document.getElementById('slider');

// creates a slider with two handles
noUiSlider.create(slider, {
  // starting values for both handles
  start: [yearMin, yearMax],
  // colors the sections between the handles
  connect: [false, true, false],
  // shows the values
  tooltips: [true, true],
  // gets rid of the decimals
  format: {
    // 'to' the formatted value. Receives a number.
    to: (value) => {
      return Math.round(value);
    },
    // 'from' the formatted value.
    // Receives a string, should return a number.
    from: (value) => {
      return Number(value);
    },
  },
  // determines the range that values can take
  range: {
    'min': [yearMin],
    'max': [yearMax],
  },
});

// ---- to merge the slider's tooltips when close together ----
const tooltips = slider.noUiSlider.getTooltips();
const origins = slider.noUiSlider.getOrigins();

// Move tooltips into the origin element.
// The default stylesheet handles this.
tooltips.forEach(function(tooltip, index) {
  if (tooltip) {
    origins[index].appendChild(tooltip);
  }
});

// values: Current slider values (array);
// handle: Handle that caused the event (number);
// unencoded: Slider values without formatting (array);
// tap: Event was caused by the user tapping the slider (boolean);
// positions: Left offset of the handles (array);
slider.noUiSlider.on('update',
    (values, handle, unencoded, tap, positions) => {
      // merge if they are too close (< 25px)
      if ((positions[1] - positions[0]) < 25) {
        if (positions[1] != positions[0]) {
          // Copies the value of the 2nd into the first
          tooltips[0].innerHTML =
           values.join(' - ');
        } else {
          // Restore the first tooltip
          tooltips[0].innerHTML = values[0];
        }
        // Hide the second tooltip
        tooltips[1].style.display = 'none';
      } else {
        // Restore the first tooltip
        tooltips[0].innerHTML = values[0];
        // and the second tooltip
        tooltips[1].style.display = 'block';
      }
    });

// ---- automatically update the year filters ----
slider.noUiSlider.on('set', (values) => {
  searchWrapper.filterList.yearMin = values[0];
  searchWrapper.filterList.yearMax = values[1];
});


export default slider;
