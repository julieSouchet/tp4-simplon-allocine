// TODO replace it with the min version for prod
import Vue from 'vue/dist/vue.js';
import 'bootstrap';
// install carousel plugin for vue
import Siema from 'vue2-siema';
Vue.use(Siema);
// scroll-top arrow
import '../../node_modules/waypoints/lib/noframework.waypoints.min.js';
import './scrolltop.js';
// utilities functions
import {
  getFullSize, getPosterAlt, getThumbnail,
  getReleaseYear,
}
  from './constants.js';
import {
  fetchMovies, fetchTrending, fetchGenreTrending,
  emptyMovie, emptyMovieList, genreList,
} from './fetch.js';

// style
import '../sass/style.scss';


// movie for sharing between pages
let currentMovie;


// ------ custom component to display STARS ------
Vue.component('movieStar', {
  template: `<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 25 25' class='movieStar' height='94.5' width='94.5'><path d='M19.8 25l-7.6-4.5-7.8 4 1.7-8.9L0 9.1l8.7-1 4.1-8 3.6 8.3L25 9.8 18.6 16l1.2 9z'/></svg>`,
});


// ------ custom component for PAGINATION (LINKS) ------
Vue.component('pageLink', {
  // contains the necessary info to make the link work
  props: ['info'],
  // what it will be replaced with in the HTML
  // each link is an anchor to the results section
  template: `
    <li
    class='page-item mx-1'
    v-bind:class='{active: info.active}'
    v-if='info.seen'>
    <a
    class='page-link'
    href='#searchResultSection'
    v-on:click='goToPage'>
    {{info.text}}
    </a>
    </li>
    `,
  methods: {
    goToPage: function() {
      searchWrapper.page = this.info.number;
      fetchMovies();
    },
  },
});


// ------ custom component for PAGINATION (BOX) ------
Vue.component('pageBox', {
  props: ['pageLinkList'],
  // what it'll be replaced with in the HTML
  template: `
    <nav aria-label='pagination des résultats de recherche'>
    <ul class='pagination justify-content-center'>
    <!-- page links (generated with Vue) -->
    <page-link
    v-for='pl in pageLinkList'
    v-bind:info='pl'
    v-bind:key='pl.id'
    class='whiteOutline'>
    </page-link>

    </ul>
    </nav>
    `,
});


// ------ custom component to display MOVIES GENRES ------
// the component for individual genres
Vue.component('movieGenre', {
  props: ['id', 'title'],
  template: `
    <span class='badge badge-secondary mx-1' itemprop='genre'>
    {{ parseGenre() }}
    </span>`,
  methods: {
    parseGenre: function() {
      for (let i = 0; i < genreList.length; i++) {
        if (genreList[i].id == this.id) {
          return genreList[i].name;
        }
      }
      // if we get here => there is no genre with this id in the list
      throw new Error(`${this.title} has unknown genre id : ${this.id}`);
    },
  },
});

// ------ custom component to display SEARCH RESULTS ------
Vue.component('searchResult', {
  props: ['movie'],
  template: `<div
  class='searchResult row'
  itemscope itemtype='http://schema.org/Movie'
  v-on:click='setCurrentMovie'
  data-toggle="modal" data-target="#details">
    <img class='moviePoster'
    v-bind:src='getThumbnail(movie.poster_path)'
    v-bind:alt='getPosterAlt(movie.title)'
    item-prop='image'>
    <div class='col-5'>
    <h4 class='movieTitle' itemprop='name'>
    {{ movie.title }}
    </h4>
    <p class='movieYear' v-if='movie.release_date'>
    ({{ getReleaseYear(movie.release_date) }})
    </p>
    <p class='movieRating' itemprop='aggregateRating' itemscope itemtype='http://schema.org/AggregateRating'>
      <span class='sr-only' itemprop='ratingValue'>
      {{ Math.ceil(movie.vote_average/2) }}
      </span>
      <movie-star
      v-for='i in Math.ceil(movie.vote_average/2)'
      v-bind:key='i'>
      </movie-star>
    </p>
    <p class='movieGenre'>
      <movie-genre
      v-for='genre in movie.genre_ids'
      v-bind:id='genre'
      v-bind:title='movie.title'
      v-bind:key='genre'>
      </movie-genre>
    </p>
    </div>
    <p class='movieOverview hiddenBelowMd truncateOverflow col'
    itemprop='description'>
    {{ movie.overview }}
    </p>
    </div>`,
  methods: {
    // returns the URL to the poster or to the placeholder if no poster
    getThumbnail: getThumbnail,
    getFullSize: getFullSize,
    getPosterAlt: getPosterAlt,
    getReleaseYear: getReleaseYear,
    setCurrentMovie: function() {
      currentMovie = this.movie;
      searchWrapper.currentMovie = this.movie;
    },
  },
});


// CASTING
Vue.component('actor', {
  props: ['actor'],
  template: `
  <div class="card col-md-2">
    <img v-bind:src="getFullSize" class="card-img-top" v-bind:alt="actor.name">
    <div class="card-body>
      <p class="card-text">
        <strong>{{ actor.name }} : </strong>
        {{ actor.character }}
      </p>
    </div>
  </div>
  `,
  methods: {
    getFullSize: getFullSize,
  },
});

// build the WRAPPER
export const searchWrapper = new Vue({
  el: '#searchWrapper',
  data: {
    name: '',
    nbResults: -1,
    page: 1,
    nbPages: 0,
    resultList: [],
    // bogus movie
    currentMovie: emptyMovie,
  },
  computed: {
    // contains all the necessary info to create a pageLink component
    pageLinkList: function() {
      return {
        first: {
          id: 'first',
          active: false,
          number: 1,
          text: '<<',
          seen: this.page > 2,
        },
        prev: {
          id: 'prev',
          active: false,
          number: (this.page - 1),
          text: '<',
          seen: this.page > 1,
        },
        prevNumber: {
          id: 'prevNumber',
          active: false,
          number: (this.page - 1),
          text: (this.page - 1),
          seen: this.page > 1,
        },
        current: {
          id: 'current',
          active: true,
          number: this.page,
          text: this.page,
          seen: this.nbPages > 1,
        },
        nextNumber: {
          id: 'nextNumber',
          active: false,
          number: (this.page + 1),
          text: (this.page + 1),
          seen: this.page < this.nbPages,
        },
        next: {
          id: 'next',
          active: false,
          number: (this.page + 1),
          text: '>',
          seen: this.page < this.nbPages,
        },
        last: {
          id: 'last',
          active: false,
          number: (this.nbPages),
          text: '>>',
          seen: this.page < this.nbPages - 1,
        },
      };
    },
  },
  watch: {
    name: function() {
      this.page = 1;
      fetchMovies();
    },
  },
  methods: {
    getThumbnail: getThumbnail,
    getFullSize: getFullSize,
    getPosterAlt: getPosterAlt,
    getReleaseYear: getReleaseYear,
  },
});
// used for giving an initial value
let initMovie = sessionStorage.getItem('name');
searchWrapper.name = initMovie ? initMovie : '';
initMovie = JSON.parse(sessionStorage.getItem('movie'));
searchWrapper.currentMovie = initMovie ? initMovie : emptyMovie;


// ---------- index.html ----------

// --------- TRENDING CAROUSEL ---------

// initializing the Vue instance
export const carouselWrapper = new Vue({
  el: '#carouselWrapper',
  data: {
    // carousel data
    currentSlide: 0,
    options: {
      perPage: {
        // display one item on small and - screens
        1: 1,
        // display 3 on medium and + screens
        768: 3,
        // display 5 on extra-large
        1200: 5,
      },
      duration: 1000,
    },
    playing: true,
    ready: false,
    // movies data
    movieList: emptyMovieList,
    currentMovie: emptyMovie,
    clickPosition: null,
  },
  methods: {
    getFullSize: getFullSize,
    getPosterAlt: getPosterAlt,
    getReleaseYear: getReleaseYear,
    /**
     * Memorizes the position of the cursor click
     * @param {Event} e what triggered the function
     */
    startSetting: function(e) {
      this.clickPosition = {
        x: e.clientX,
        y: e.clientY,
      };
    },
    /**
     * If the event wasn't a 'drag', opens the modal for this movie
     * @param {JSON} movie the movie that was clicked
     * @param {Event} e the triggering event
     */
    setCurrentMovie: function(movie, e) {
      if (this.clickPosition &&
        e.clientX == this.clickPosition.x &&
        e.clientY == this.clickPosition.y) {
        // if there was a click w/o dragging
        // => execute the opening of the modal as usual
        currentMovie = movie;
        this.currentMovie = movie;
      } else {
        // if there was dragging
        // => prevents the modal from opening
        e.stopImmediatePropagation();
      }
    },
  },
  // watch: {
  //   // debug : display slide change
  //   currentSlide: (val, oldVal) => {
  //     console.log(`slide ${oldVal} -> slide ${val}`);
  //   },
  // },
});

fetchTrending();

// --------- GENRE TABS ---------
Vue.component('tabItem', {
  props: {
    // the component's genre
    genre: {
      default: {name: 'test', id: 0},
    },
    // the wrapper's genre <=> the current active tab
    currentGenre: {
      default: {name: 'action', id: 28},
    },
  },
  template: `
  <li v-on:click="setGenre">
    <a v-bind:class="['nav-link', { active : isActive}]"
    href="#genreWrapper">{{ genre.name }}</a>
  </li>`,
  methods: {
    setGenre() {
      genreWrapper.currentGenre = this.genre.id;
    },
  },
  computed: {
    isActive() {
      return this.currentGenre == this.genre.id;
    },
  },
});

export const genreWrapper = new Vue({
  el: '#genreWrapper',
  data: {
    // the id of the genre currently shown
    // starts with 'action'
    currentGenre: 28,
    movieList: emptyMovieList,
    // a selection of genres, with their TMDb ids
    tabTitles: [
      {name: 'Action', id: 28},
      {name: 'Comédie', id: 35},
      {name: 'Thriller', id: 53},
      {name: 'Romance', id: 10749},
      {name: 'Science-fiction', id: 878},
    ],
    baseClass: 'nav-link',
    activeClass: 'active',
  },
  methods: {
    setGenre: function(genre) {
      console.log('genre : ' + genre);
      this.currentGenre = genre;
    },
    getThumbnail: getThumbnail,
    getFullSize: getFullSize,
    getPosterAlt: getPosterAlt,
  },
  watch: {
    currentGenre: function() {
      fetchGenreTrending(this.currentGenre);
    },
  },
});

// initialisation
fetchGenreTrending(genreWrapper.currentGenre);


// ----- INDEX SEARCH BAR -----
/**
 * We store the movie's name in sessionStorage and go to the research page
 * @param {String} text the movie's name, to be transmitted
 */
const searchMovie = (text) => {
  // we read the movie's name and store it
  sessionStorage.setItem('name', text);
  // we go to the other page
  window.location = 'recherche#searchResultSection';
};

const searchBar = document.getElementById('searchBar');
searchBar.addEventListener('keydown', (e) => {
  if (e.key == 'Enter') {
    searchMovie(searchBar.value);
  }
});
document.getElementById('searchButton').addEventListener('click', () => {
  searchMovie(searchBar.value);
});


// -------- MOVIE DETAILED PAGE -------
/**
 * We store the movie in sessionStorage and go to the detail page
 * @param {JSON} movie the movie
 */
const searchMovieDetails = (movie) => {
  // we read the movie's name and store it
  sessionStorage.setItem('movie', JSON.stringify(movie));
  // we go to the other page
  window.location = 'film';
};

// clicking on the modal body will open the page for this specific movie
Array.from(document.getElementsByClassName('modal-body'))[0]
    .addEventListener('click', () => {
      // if (carouselWrapper) {
      //   // index.html
      //   searchMovieDetails(carouselWrapper.currentMovie);
      // } else {
      //   // recherche.html
      //   searchMovieDetails(searchWrapper.currentMovie);
      // }
      searchMovieDetails(currentMovie);
    });
