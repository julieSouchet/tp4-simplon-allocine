
/* eslint-disable no-unused-vars */
const scrollTop = document.getElementById('scrollTop');

// hides the scrollTop arrow when near the top of the page
const scrollTopPopUp = new Waypoint({
  element: document.getElementById('viewTop'),
  handler: function(direction) {
    if (direction == 'down') {
      scrollTop.classList.remove('disabled');
      scrollTop.classList.toggle('show');
    } else {
      scrollTop.classList.add('disabled');
      scrollTop.classList.toggle('show');
    }
  },
  offset: '-10%',
});


/* when at the bottom of the page,
pushes the scroll top arrow slightly up so it isn't on the footer */
const scrollTopSticky = new Waypoint({
  element: document.getElementById('viewBottom'),
  handler: function(direction) {
    if (direction == 'down') {
      scrollTop.style['bottom'] = '70px';
    } else {
      scrollTop.style['bottom'] = '5px';
    }
  },
  offset: 'bottom-in-view',
});
