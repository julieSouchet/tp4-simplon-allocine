// useful assets
export const imgBasePath = 'https://image.tmdb.org/t/p';
export const imgAssetsPath = '../assets/img/noPoster.jpg';
export const posterThumbnailSize = '/w154';
export const posterSize = '/original';
// the current year
export const yearMax = new Date().getFullYear();
// (internet tells me the first movie was created in 1878)
export const yearMin = 1878;

export const getThumbnail = (path) =>
  path ? imgBasePath + posterThumbnailSize + path :
  imgAssetsPath;
export const getFullSize = (path) =>
  path ? imgBasePath + posterSize + path :
  imgAssetsPath;
export const getPosterAlt = (title) => 'poster ' + title;
export const getReleaseYear = (d) => {
  const date = new Date(d);
  return date.getFullYear();
};
